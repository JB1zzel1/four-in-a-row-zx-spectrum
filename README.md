# Four In A Row ZX Spectrum

## Connect 4 for the ZX Spectrum

## Description
My first ASM programme


## Visuals
See screen shot

## Installation
Use with PASMO - compile to create a tap file or tzx. Then play on an emulator.

Use a command like this:

pasmo -1 --tapbas pete2.asm pete2.tap >> debug.txt

## Roadmap
I want to add a check to see if someone has won.


## Project status
active 
