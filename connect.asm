org 30000
restart:
; paint the game screen
call 3503 ;clear screen
; attribute paper black ink red is $02 I think
; attribute paper blue ink red is $0A I think
LD HL,22528 ; start of attributes
ld d,$0a ; blue
ld b,64 ; paint first 2 rows blue
paint:
ld (hl),d
inc hl
djnz paint

ld b,6
mainboard:
push bc


ld b,6  ; paint first 6 attribs blue
paint0:
ld (hl),d
inc hl
djnz paint0


ld b,7 ; paint the counter area
here:
ld d,$02 ; black
push bc

ld b,2
paint1:
ld (hl),d
inc hl
djnz paint1
ld d,$0a
ld (hl),d
inc hl

pop bc
djnz here

ld b,11 ; end of the row, and wrapt to 6 lines or next row 
paint2:
ld (hl),d
inc hl
djnz paint2

ld b,7 ; draw next counter
here0:
ld d,$02 ; black
push bc

ld b,2
paint3:
ld (hl),d
inc hl
djnz paint3
ld d,$0a
ld (hl),d
inc hl

pop bc
djnz here0

ld b,37 ; end of the row, and wrapt to 6 lines or next row 
paint4:
ld (hl),d
inc hl
djnz paint4

pop bc
djnz mainboard

ld d,$0a ; blue
ld b,128 ; paint first 2 rows blue
paint5:
ld (hl),d
inc hl
djnz paint5

;print dog in the top left of the screen
ld hl,$5800
ld (hl),2

ld b,20
ld hl,$5A86
here1:
ld (hl),15
inc l
djnz here1

ld hl,$4000
ld de,udg

call printudg

ld hl,$5087
ld de,udg

ld b,7

;add the number on the bottom, and center them with the scroll routine. Slow it down to show it off.
numbers:
ld a,e
add a,8
ld e,a
call printudg
inc l
inc l
inc l
djnz numbers

call scroll
call delay
call scroll
call delay
call scroll
call delay
call scroll
call delay
call scroll
call delay
call scroll




;
; start drawing the counter animation
;

ld h,$40

pollkey:
;read keys
ld bc,$F7FE ; the area of the keyboard
 in a,(c)
 and %00000001; press 1
 jr z,one

ld bc,$F7FE ; the area of the keyboard
 in a,(c)
 and %00000010; press 2
 jr z,two

ld bc,$F7FE ; the area of the keyboard
 in a,(c)
 and %00000100; press 3
 jr z,three

 ld bc,$F7FE ; the area of the keyboard
 in a,(c)
 and %00001000; press 4
 jr z,four

ld bc,$F7FE ; the area of the keyboard
 in a,(c)
 and %00010000; press 5
 jr z,five

ld bc,$EFFE ; the area of the keyboard
 in a,(c)
 and %00010000; press 6
 jr z,six

ld bc,$EFFE ; the area of the keyboard
 in a,(c)
 and %00001000; press 7
 jr z,seven

ld bc,$EFFE ; the area of the keyboard
 in a,(c)
 and %0000001; press 
call delay
 jp z, restart

call delay
jp pollkey


one:
ld l,$46
jp go
two:
ld l,$49
jp go
three:
ld l,$4c
jp go
four:
ld l,$4f
jp go
five:
ld l,$52
jp go
six:
ld l,$55
jp go
seven:
ld l,$58
jp go


go:

;call switchplayer
call paintcolumn


push hl

call drop
pop hl
call switchplayer

jp pollkey




drop:

 ; load screen address where you want to draw the square

;push hl
; the top of the screen, if there is already a counter here it should not do anything
; check to see if (hl) = (255) and jump to skipdrop so its not printed.
;pop hl

ld a,(hl)
push hl
ld b,255
xor b 
;jr z, skipdrop

jr nz, skipd0
ret
skipd0
call counter

pop hl 
push af
ld a,l ;ld hl, $40A1
add a,96
ld l,a 
pop af
push HL
call counter

POP HL
push af
ld a,l ;$4801
add a,96
ld l,a 
ld h,$48
pop af
push HL
call counter

pop HL
push af
ld a,l ;ld hl, $48c1
add a,96
ld l,a 
pop af
push HL
call counter

pop HL
push af
ld a,l ;ld hl, $48c1
add a,96
ld l,a 
pop af
push HL
call counter
pop hl

;skipdrop:
ret

counter:
push hl

ld de,block   ; store source
ld c,2

drawudg: ; draw the block 1x2 attributes in size
; otherwize draw the sprite
ld b,8      ; loop 8 times
ld a,(de)   ; load source

loop:    ; loop back here

; check to see if (hl) = (255) and jump to skipprint to avoid the delay
push af
push bc
ld a,(hl)
ld b,255
xor b 
jr z, skipprint
call delay ; slow down so its animated

; jump here if the block already contains something
skipprint:
pop bc
pop af

ld (hl),a   ; copy to screen
inc l
ld (hl),a   ; copy to screen
dec l

inc h


       ; next line of char
DJNZ loop   ; b=b-1, if b <> 0 loop.

pop hl ; next attribute row
ld a,l
add a,32
ld l,a
push hl

dec c
jr nz, drawudg ; loop back to draw a 2*2 attribute square


;
; check to see if there is already a counter
;

ld d,$10 ; loop counter, but also the address to check

loop5
pop hl
push hl  ; get the last screen address where we finished last time

ld a,l    ; see if location we want to move to is on a screen third.
and $f0
;xor $00
xor d
jr nz, skipdrop0

ld a,h
add a,$08
ld h,a

ld a,l
add a,$20
ld l,a

; check for counter
ld a,(hl)
ld b,255
xor b 
jp nz, skipdrop0

pop hl
ret
skipdrop0:


ld a,d
sub $10
; is it D0? if so quit,
ld d,a

xor $d0
jp nz,loop5

; now test if there is no screen thrid to deal with.
pop hl
push hl  ; get the origianl screen address from call

ld a,l
add a,$20
ld l,a

; check for counter
ld a,(hl)
ld b,255
xor b 
jp nz, skipdrop1

pop hl
ret
skipdrop1:

; remove the top 8 lines of the square I just drew
; one at a time as an animation

ld de,blank ; load the blank line

pop hl

ld a,l ; recover original screen address
sub 64
ld l,a
push hl

ld b,8      ; loop 8 times
ld a,(de)   ; load source
loop1:    ; loop back here
ld (hl),a   ; copy to screen
inc l
ld (hl),a   ; copy to screen
dec l
call delay
inc h
       ; next line of char
DJNZ loop1   ; b=b-1, if b <> 0 loop.

pop hl ; get start address on hl
ld a,l
add a,32 ; move down 1 attribute
ld l,a
push hl


; This bit deletes a line, and then draws a line 2 attributs below, to create 
; an effect like the square is moving behind something

; get loop ready
ld b,8  

loop2:
;
; get the blank line into A
;
ld a,(de)
;
; here is where you blank the first line,
;
ld (hl),a   ; copy to screen
inc l
ld (hl),a   ; copy to screen
dec l
;
; then change UDG
;
ld de,block
;
; then set up new loctation - 2 attributes below current location
;


;this is where is can cross the 3rds screen boundry. so need to check HL and amend
; so L will be C somthing. need to check if L  > 192
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $C0 ; if a = $c then 0
jr nz, areacheck ;if not 0 then skip next bit of code

;move to next 3rd of screen
pop hl ; next line
ld a,h
add a,8
ld h,a
push hl  
areacheck:

; so L will be E somthing. need to check if L  > 192
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $E0 ; if a = $c then 0
jr nz, areacheck3 ;if not 0 then skip next bit of code

;move to next 3rd of screen
pop hl ; next line
ld a,h
add a,8
ld h,a
push hl  
areacheck3:



; second half of screen


;this is where is can cross the 3rds screen boundry. so need to check HL and amend
; so L will be C somthing. need to check if L  > 192
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $D0 ; if a = $c then 0
jr nz, check ;if not 0 then skip next bit of code

;move to next 3rd of screen
pop hl ; next line
ld a,h
add a,8
ld h,a
push hl  
check:


; so L will be E somthing. need to check if L  > 192
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $F0 ; if a = $c then 0
jr nz, check6 ;if not 0 then skip next bit of code

;move to next 3rd of screen
pop hl ; next line
ld a,h
add a,8
ld h,a
push hl  
check6:


pop hl ; next line
ld a,l
add a,64
ld l,a
push hl  
;



;print new udg
;
ld a,(de)
ld (hl),a   ; copy to screen
inc l
ld (hl),a   ; copy to screen
dec l
;
; change UDG back
;
ld de,blank
;
;go back to other orinial address
;


;this is where is can cross the 3rds screen boundry. so need to check HL and amend
; so L will be 2 somthing. need to check if L  to see where we are
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $20
jr nz, areacheck1 ;if not 0 then skip next bit of code

;move to next 3rd of screen
pop hl ; next line
ld a,h
sub 8
ld h,a
;ld h,$40
push hl  
areacheck1:


ld a,l
and $F0 ; I just want the first 4 bits of L
jr nz, areacheck4 ;if not 0 then skip next bit of code

;move to next 3rd of screen
pop hl ; next line
ld a,h
sub 8
ld h,a
;ld h,$40
push hl  
areacheck4:
; second half of screen
;this is where is can cross the 3rds screen boundry. so need to check HL and amend
; so L 
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $30 ; if a = $c then 0
jr nz, check1 ;if not 0 then skip next bit of code
;move to next 3rd of screen
pop hl ; next line
ld a,h
sub 8
ld h,a
;ld h,$40
push hl  
check1:
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $10 ; if a = $c then 0

jr nz, check4 ;if not 0 then skip next bit of code
;move to next 3rd of screen
pop hl ; next line
ld a,h
sub 8
ld h,a
;ld h,$40
push hl  
check4:

pop hl ; next line
ld a,l
sub 64
ld l,a
;
;move to next row
;
inc h
push hl
;
;loop this 8 times
;
call delay
;DJNZ loop2
; djnz out of range
dec b
jp nz, loop2


pop hl 
; next row to draw
ld a,h ; recover original screen address (sort of)
sub 8
ld h,a
;ld hl, $4021 ; load screen address

ld a,l
add a,96 ; the location where the bottom half of the square needs to be drawn.
ld l,a

; check if we are on line $20 to $2F
push af
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $20
jr nz, areacheck2 ;if not 0 then skip next bit of code

;move to next 3rd of screen
;pop hl ; next line
;ld a,h
;add a,16
;ld h,a
ld h,$48
;push hl  
areacheck2:
pop af


; check if we are on line $20 to $2F
push af
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $40
jr nz, areacheck5 ;if not 0 then skip next bit of code

;move to next 3rd of screen
;pop hl ; next line
;ld a,l
;add a,64
;ld l,a
ld h,$50
;push hl  
areacheck5:
pop af

; second half of screen

; check if we are on line $20 to $2F
push af
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $30
jr nz, area2 ;if not 0 then skip next bit of code

;move to next 3rd of screen
ld h,$48  
area2:
pop af

; check if we are on line $20 to $2F
push af
ld a,l
and $F0 ; I just want the first 4 bits of L
xor $50
jr nz, area5 ;if not 0 then skip next bit of code

;move to next 3rd of screen
ld h,$50
area5:
pop af


;just draw 8 lines of the square - easy now.

ld b,8      ; loop 8 times
ld de,block
ld a,(de)   ; load source

loop3:    ; loop back here
ld (hl),a   ; copy to screen
inc l
ld (hl),a   ; copy to screen
dec l
call delay
inc h
; next line of char
DJNZ loop3   ; b=b-1, if b <> 0 loop.



ret


paintcolumn:
push HL

ld b,6
looppc:
push bc

; is there a counter there already?
ld a,(hl)
xor 255 
jp nz,endpaint
pop bc
pop hl
ret ; yes, then stop right there budy.
endpaint
call paintcounter

ld a,l
add a,$60
jp nc,carry 
push af
ld a,h
add a,$08
ld h,a
pop af
carry:
ld l,a

pop bc
djnz looppc

pop hl
ret



paintcounter:

push hl
push af
push de

; change bmp location to attribute location
; if H = 40 then
ld a,h
xor $40
jp nz,next
ld h,$58
next:
; if H = 48 then
ld a,h
xor $48 
jp nz,next0
ld h,$59
next0:
; if H = 50 then
ld a,h
xor $50
jp nz,next1
ld h,$5A
next1:

call setcolour
inc l
call setcolour
ld a,l
add a,32 ; the row below it
ld l,a
call setcolour
dec l
call setcolour

pop de
pop af
pop hl

ret


setcolour
push hl
ld hl,$5800
ld d,(hl)
pop hl
ld (hl),d
ret


switchplayer:
;chang border colour here


;
; check colour 
;

push HL
LD HL,22528 ; start of attributes
ld a,(hl)
xor 2
jp z,yellow

;
; Red
;
ld d,2 ; red
ld (hl),d
ld a,%00000010 ; 
 call 8859 ; set border colours.
jp checkplayerend

;
; Yellow
;
yellow:
ld d,6 ; yellow
ld (hl),d
ld a,%00000110 ; 
 call 8859 ; set border colours.


checkplayerend:
pop HL
ret


printudg:
push bc
push hl
push de
ld b,8
print:
ld a,(de)
ld (hl),a
inc de
inc h
djnz print
pop de
pop hl
pop bc
ret


delay:
push bc
ld b,8 ; length of delay.
delay01
push bc
ld b,255 ; length of delay.
delay0: 
;halt ; wait for an interrupt.
djnz delay0 ; loop.
pop bc
djnz delay01 ; loop.
pop bc

ret 

scroll
LD HL,$509F
L7E0D:
       LD C,$08          ; load c with 8 (setting up a loop)
L7E0F:
       XOR A             ; set A to zero
       PUSH HL           ; put HL on the stack, from the call earlier HL is right edge, 3 attr down: #405F
       LD DE,$001F       ; load de 31 (number of attributes in a row on the screen)
       SBC HL,DE         ; subtract DE from HL with carry  4005-001f = left edge, 3 attr down: #4040F
       LD A,(HL)         ; load A with whats at the HL location - ie left edge 3 attributes down #4040F
       ADD HL,DE         ; Add DE (31) to HL - move back to the right edge of the screen #405F
       RLA               ; rotate left acumilator outcome: move the left most pixel on to the carry flag

       LD B,$20          ; Loop 32 times
L7E1B: 
       LD A,(HL)         ; load A with whats at HL ie a row of 8 pixels at the right of the sceen
       RLA               ; rotate left acumilator - puts the pixel from the left of screen, on the right of the sceen, and moves pixels in the Attr 1 to the left and so on 
       LD (HL),A         ; draw it on the screen.
       DEC HL            ; hl = hl -1 : #405F - 1 = #405E. i.e., move one attribute to the left.
       DJNZ L7E1B        ; B=b-1. If B <>0 go to L7e1B ie do this 32 times

       POP HL                 ; from the call earlier HL is right edge, 3 attr down: #405F
       INC H                  ; next line of char
       DEC C                  ; c=c-1 (C was 8, so for each of the 8 rows of pixels)
       JR NZ,L7E0F       ; If C is not 0 loop.
       RET                    ; Return



udg: 
defb $3C,$7E,$99,$99,$FF,$81,$42,$3C ; face
;defb $10, $F0, $D1, $FF, $7F, $7F, $7F, $55 ; a dog

; nums 1-7
defb $4,$C,$4,$4,$4,$4,$4,$0
defb $E,$11,$1,$2,$C,$10,$1F,$0
defb $E,$11,$1,$6,$1,$11,$E,$0
defb $2,$6,$A,$12,$1F,$2,$2,$0
defb $1F,$10,$1E,$1,$1,$11,$E,$0
defb $E,$11,$10,$1E,$11,$11,$E,$0
defb $F,$1,$1,$2,$4,$8,$8,$0


block:
DEFB 255

blank:
DEFB 0


end 30000
